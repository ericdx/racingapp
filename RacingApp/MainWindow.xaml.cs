﻿using Autofac;
using ClassLibrary;
using ClassLibrary.Interfaces;
using ClassLibrary.Vehicles;
using Newtonsoft.Json;
using RaceEngine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace RacingApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Autofac.IContainer _container;
        IEngine _engine;
        private BackgroundWorker _backgroundWorker;
        private IRaceConfiguration _config;

        public MainWindow()
        {
            InitializeComponent();
            var builder = new ContainerBuilder();
            builder.RegisterType<RaceEngine.RaceEngine>().As<IEngine>();
            builder.RegisterType<RaceConfiguration>().As<IRaceConfiguration>();
            _container = builder.Build();
            _engine = _container.Resolve<IEngine>();
            _config = _container.Resolve<IRaceConfiguration>();
            _config = _config.GetConfig();
            lblMembers.Content = $"Участников гонки: {_config.VehiclesInRace.Count}, длина круга {_config.RacingLapLength}km";
            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.WorkerReportsProgress = true;
            _backgroundWorker.DoWork += _backgroundWorker_DoWork;
            _backgroundWorker.ProgressChanged += _backgroundWorker_ProgressChanged;
            _backgroundWorker.RunWorkerCompleted += _backgroundWorker_RunWorkerCompleted;

        }

        private void _backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnStart.IsEnabled = true;
            _config.VehiclesInRace.ForEach(vehicle =>
            {
                vehicle.StartRaceEvent -= Vehicle_ReportRacingState;
                vehicle.ReportRacingState -= Vehicle_ReportRacingState;
            });
        }

        private void _backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            LogActivity(e.UserState as string);
        }

        private void _backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            _config.VehiclesInRace.ForEach(vehicle =>
            {
                vehicle.StartRaceEvent += Vehicle_ReportRacingState;
                vehicle.ReportRacingState += Vehicle_ReportRacingState;
            });
            var champions = _engine.StartEmulation(_config);

            _backgroundWorker.ReportProgress(100, "*** Таблица первенства ***");
            int i = 1;
            champions.ForEach(ch =>
            {
                _backgroundWorker.ReportProgress(100, $" {i++} место {ch}");
            });

        }

        private void Vehicle_ReportRacingState(object sender, string message)
        {
            _backgroundWorker.ReportProgress(0, message);
        }


        private void LogActivity(string message)
        {
            lbActivity.Items.Add(new ListBoxItem { Content = message });
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            lbActivity.Items.Clear();
            btnStart.IsEnabled = false;

            _backgroundWorker.RunWorkerAsync();

        }

    }
}
