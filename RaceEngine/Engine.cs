﻿using ClassLibrary;
using ClassLibrary.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace RaceEngine
{
    public class RaceEngine : RaceEvents, IEngine
    {
        static object _finishedLock = new object();
        List<string> _champions = new List<string>();
        public List<IVehicle> VehiclesInRace { get; set; }

        private void LapFinishedEventHandler(object sender, string vehicleName)
        {
            lock (_finishedLock)
                _champions.Add(vehicleName);
        }
        public List<string> StartEmulation(IRaceConfiguration config)
        {
            _champions.Clear();
            List<Task> tasks = new List<Task>();
            VehiclesInRace = config.VehiclesInRace;
            VehiclesInRace.ForEach(vehicle =>
            {
                tasks.Add(Task.Run(() =>
               {
                   vehicle.LapFinished += LapFinishedEventHandler;
                   vehicle.Drive(config.RacingLapLength);
               }));
            });

            base.StartTheRace();
            Task.WaitAll(tasks.ToArray());
            VehiclesInRace.ForEach(vehicle =>
            {
                vehicle.LapFinished -= LapFinishedEventHandler;
            });
            return _champions;
        }
    }
}
