﻿using ClassLibrary.Interfaces;
using System;
using System.Text;
using System.Threading;

namespace ClassLibrary
{
    public abstract class Vehicle : RaceEvents, IVehicle
    {
        protected int vehicleNumber;
        int _velocity;
        double _blowoutProbability;
        private double _speedPerSecond;
        const int _reportInterval = 3;
        string vehicleName;

        public Vehicle(int velocity, double blowoutProbability, int vehicleNumber)
        {
            _velocity = velocity;
            _blowoutProbability = blowoutProbability;
            _speedPerSecond = _velocity / 3600.0;
            vehicleName = $" TC #{vehicleNumber} ";
        }

        public virtual void Drive(double racingLapLength)
        {
            const int repairTireSeconds = 5;
            double movedKm = 0;

            if (_blowoutProbability > 0)
            {
                var blowoutPredictor = new Random();
                while (movedKm < racingLapLength)
                {
                    var blowoutEvent = blowoutPredictor.NextDouble();
                    if (blowoutEvent <= _blowoutProbability)
                    {
                        var blowPoint = (racingLapLength - movedKm) * blowoutEvent;
                        var secondsBeforeBlow = blowPoint / _speedPerSecond;
                        movedKm = DriveToPoint(secondsBeforeBlow, movedKm);
                        ReportRacingStateSignal($" {vehicleName}: прокол колеса на { movedKm.ToString("N")} км. Ремонт {repairTireSeconds} сек..");
                        Thread.Sleep(1000 * repairTireSeconds);
                    }
                    else
                         movedKm = DriveToPoint((racingLapLength - movedKm) / _speedPerSecond, movedKm);
                }
            }
            else
            {
                DriveToPoint(racingLapLength / _speedPerSecond, movedKm);
            }
            ReportFinishedLap(vehicleName);
        }


        private double DriveToPoint(double secondsToDrive, double movedKm)
        {
            int i = 1;
            for (; i < (int)secondsToDrive / _reportInterval; i++)
            {
                Thread.Sleep(1000 * _reportInterval);
                movedKm += _speedPerSecond * _reportInterval;
                ReportRacingStateSignal(
                    $" {vehicleName} прошло { movedKm.ToString("N")} км");
            }
            var leftToGo = secondsToDrive - i * _reportInterval;
            if (leftToGo < 0)
                leftToGo = secondsToDrive;

            Thread.Sleep(Convert.ToInt32(1000 * leftToGo));
            movedKm += leftToGo * _speedPerSecond;

            return movedKm;
        }

        protected override void ReportParameters(string customParameter)
        {
            var sb = new StringBuilder(customParameter);
            sb.Append($" ; Скорость: {_velocity} км/ч ; Вероятность прокола колеса: {_blowoutProbability}");
            base.ReportParameters(sb.ToString());
        }
    }
}
