﻿
namespace ClassLibrary.Vehicles
{
    public class Bike : Vehicle
    {
        bool _hasCradle;
        public Bike(int number, int velocity, double blowoutProbability, bool cradle) 
            : base(velocity,  blowoutProbability,number)
        {
            _hasCradle = cradle;           
        }
        public override void Drive(double racingLapLength)
        {
            WaitForStart();
            var param = _hasCradle ? "есть" : "нет";
            ReportParameters($"Коляска: {param}");
            base.Drive(racingLapLength);
        }       
    }
}
