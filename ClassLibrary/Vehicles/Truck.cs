﻿
namespace ClassLibrary.Vehicles
{
    public class Truck : Vehicle
    {
        decimal _weight;
        public Truck(int number, int velocity, double blowoutProbability, decimal weight) 
            : base(velocity,  blowoutProbability,number)
        {
            _weight = weight;           
        }
        public override void Drive(double racingLapLength)
        {
            WaitForStart();
            ReportParameters($"Вес груза: {_weight}");
            base.Drive(racingLapLength);
        }       
    }
}
