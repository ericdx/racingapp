﻿
namespace ClassLibrary.Vehicles
{
    public class Auto : Vehicle
    {
        int _passengers;
        public Auto(int number, int velocity, double blowoutProbability, int passengers) 
            : base(velocity,  blowoutProbability,number)
        {
            _passengers = passengers;            
        }
        public override void Drive(double racingLapLength)
        {
            WaitForStart();
            ReportParameters($"Число пассажиров: {_passengers}");
            base.Drive(racingLapLength);
        }       
    }
}
