﻿using System.Collections.Generic;

namespace ClassLibrary.Interfaces
{
    public interface IRaceConfiguration
    {
         double RacingLapLength { get; set; }

         List<IVehicle> VehiclesInRace { get; set; }
        IRaceConfiguration GetConfig();
    }
}