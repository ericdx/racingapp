﻿namespace ClassLibrary.Interfaces
{
    public interface IVehicle : IEvents
    {
        void Drive(double racingLapLength);
    }
}