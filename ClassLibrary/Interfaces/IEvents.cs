﻿using System;

namespace ClassLibrary.Interfaces
{
    public interface IEvents
    {
        event EventHandler<string> StartRaceEvent;
        event EventHandler<string> ReportRacingState;
        event EventHandler<string> LapFinished;
    }
}