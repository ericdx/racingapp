﻿using System;
using System.Collections.Generic;

namespace ClassLibrary.Interfaces
{
    public interface IEngine : IEvents
    {
        List<IVehicle> VehiclesInRace { get; set; }

        List<string> StartEmulation(IRaceConfiguration config);
    }
}