﻿using ClassLibrary.Interfaces;
using ClassLibrary.Vehicles;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ClassLibrary
{
    public class RaceConfiguration : IRaceConfiguration
    {
        public double RacingLapLength { get; set; }

        public List<IVehicle> VehiclesInRace { get; set; }

        public IRaceConfiguration GetConfig()
        {
            const string configFile = "config.cfg";
            if (!File.Exists(configFile))
                throw new Exception("no config!");

            var cfg = File.ReadAllLines(configFile).ToList();
            RacingLapLength = double.Parse(cfg[0]);
            VehiclesInRace = new List<IVehicle>();
            int number = 1;
            cfg.Skip(1).ToList().ForEach(v =>
            {
                var p = v.Split(';');
                int velocity = int.Parse(p[1]);
                double blow = double.Parse(p[2]);
                switch (p[0])
                {
                    case nameof(Auto):
                        var auto = new Auto(number++, velocity, blow, int.Parse(p[3]));
                        VehiclesInRace.Add(auto);
                        break;
                    case nameof(Truck):
                        var vehicle = new Truck(number++, velocity, blow, decimal.Parse(p[3]));
                        VehiclesInRace.Add(vehicle);
                        break;
                    case nameof(Bike):
                        var bike = new Bike(number++, velocity, blow, bool.Parse(p[3]));
                        VehiclesInRace.Add(bike);
                        break;
                    default:
                        throw new Exception("undefined vehicle type!");
                }

            });
            return this;
        }
    }
}