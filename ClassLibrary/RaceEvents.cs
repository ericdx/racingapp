﻿using ClassLibrary.Interfaces;
using System;
using System.Threading;

namespace ClassLibrary
{
    public abstract class RaceEvents : IEvents
    {
        public event EventHandler<string> StartRaceEvent;
public event EventHandler<string> ReportRacingState;
public event EventHandler<string> LapFinished;
         static readonly ManualResetEvent _signalToStart = new ManualResetEvent(false);
        protected void StartTheRace()
        {
            _signalToStart.Set();
        }
 protected void WaitForStart()
        {
            _signalToStart.WaitOne();
        }
        protected virtual void ReportParameters(string allParameters)
        {
           
            StartRaceEvent.Invoke(this,allParameters);
        }
        protected void ReportRacingStateSignal(string state)
        {
            ReportRacingState.Invoke(this, state);
        }
        protected void ReportFinishedLap(string vehicleName)
        {
            LapFinished.Invoke(this, vehicleName);
        }
    }
}